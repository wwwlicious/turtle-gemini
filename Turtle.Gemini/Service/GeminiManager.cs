﻿//  Copyright 2013 Scott Mackay
//   
//  Licensed under the Apache License, Version 2.0 (the "License"); you may not use 
//  this file except in compliance with the License. You may obtain a copy of the 
//  License at 
//  
//      http://www.apache.org/licenses/LICENSE-2.0 
//  
//  Unless required by applicable law or agreed to in writing, software distributed 
//  under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
//  CONDITIONS OF ANY KIND, either express or implied. See the License for the 
//  specific language governing permissions and limitations under the License.

namespace Turtle.Gemini
{
    using System.Collections.Generic;
    using System.Linq;

    using Countersoft.Gemini.Api;
    using Countersoft.Gemini.Commons.Dto;
    using Countersoft.Gemini.Commons.Entity;

    using Turtle.Gemini.Models;

    public class GeminiManager
    {
        private readonly ServiceManager serviceManager;

        private UserDto user;

        public GeminiManager(GeminiConnection model)
        {
            if (model.UseWindowsAuthentication)
            {
                this.serviceManager = new ServiceManager(model.GeminiUrl.ToString());
            }
            else
            {
                serviceManager = new ServiceManager(model.GeminiUrl.ToString(), model.Username, null, model.ApiKey, false);
            }

            try
            {
                user = serviceManager.Admin.WhoAmI();
            }
            catch
            {
                user = null;
            }
        }

        public bool IsConnected()
        {
            try
            {
                return user != null && user.Entity.Id != 0;
            }
            catch
            {
                return false;
            }
        }
        
        public IEnumerable<GeminiIssue> GetIssues()
        {
            var filter = new IssuesFilter()
                             {
                                 Active = true,
                                 Projects = "All",
                                 IncludeClosed = false,
                                 Resources = user.Entity.Id.ToString()
                             };

            return
                serviceManager.Item.GetFilteredItems(filter)
                              .Select(
                                  x =>
                                  new GeminiIssue
                                      {
                                          Id = x.Entity.Id,
                                          Code = x.ProjectCode,
                                          Title = x.Title,
                                          Description = x.Description,
                                          Status = x.Status,
                                          Resolution = x.Resolution,
                                          Priority = x.Priority,
                                          Type = x.Type,
                                          DueDate = x.DueDate,
                                          Resources = x.ResourceNames,
                                          GeminiProject =
                                              new GeminiProject
                                                  {
                                                      Id = x.Project.Id,
                                                      Name = x.Project.Name,
                                                      Sprint = x.FixedInVersion
                                                  }
                                      });
        }
    }
}