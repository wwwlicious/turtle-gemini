﻿namespace Turtle.Gemini.Models
{
    public class GeminiProject
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Sprint { get; set; }
    }
}