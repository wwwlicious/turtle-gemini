
setlocal

if exist ./bin/Debug/Turtle.Gemini.dll (
echo generating regfile for debug build

regasm ./bin/Debug/Turtle.Gemini.dll /codebase /regfile:Turtle_Gemini_Debug.reg /silent

echo. >> Turtle_Gemini_Debug.reg
echo [HKEY_CLASSES_ROOT\CLSID\{2E64D6C6-E65C-4358-A1DC-D48DC5860054}\Implemented Categories\{3494FA92-B139-4730-9591-01135D5E7831}] >> Turtle_Gemini_Debug.reg

echo regfile for debug build is Turtle_Gemini_Debug.reg
) else (
echo debug build not found
)

if exist /Bin/Release/Turtle.Gemini.dll (
echo generating regfile for release build

regasm ../Build/Release/Turtle.Gemini.dll /codebase /regfile:Turtle_Gemini_Release.reg /silent

echo. >> Turtle_Gemini_Release.reg
echo [HKEY_CLASSES_ROOT\CLSID\{2E64D6C6-E65C-4358-A1DC-D48DC5860054}\Implemented Categories\{3494FA92-B139-4730-9591-01135D5E7831}] >> Turtle_Gemini_Release.reg

echo regfile for release build is Turtle_Gemini_Release.reg
) else (
echo release build not found
)

endlocal

pause