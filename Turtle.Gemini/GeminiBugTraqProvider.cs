﻿namespace Turtle.Gemini
{
    using System;
    using System.Diagnostics;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;

    using Newtonsoft.Json;

    using Turtle.Gemini.Models;

    // Original IDL definition can be found at:
    // http://tortoisesvn.tigris.org/svn/tortoisesvn/trunk/contrib/issue-tracker-plugins/inc/IBugTraqProvider.idl
    //
    // See also:
    // http://tortoisesvn.net/docs/release/TortoiseSVN_en/tsvn-ibugtraqprovider.html#tsvn-ibugtraqprovider-1
    [ComVisible(true), Guid(Constants.BugTraqPluginId), ClassInterface(ClassInterfaceType.None)]
    public class GeminiBugTraqProvider : IBugTraqProvider2
    {
        public GeminiBugTraqProvider()
        {
#if DEBUG
            Debugger.Launch();
#endif
            geminiConnection = new GeminiConnection();
            geminiConnection.Load();
            geminiManager = new GeminiManager(geminiConnection);
        }

        private GeminiConnection geminiConnection;

        private GeminiManager geminiManager;

        public string GetLinkText(IntPtr hParentWnd, string parameters)
        {
            /* The plugin can provide a string here which is used in the TortoiseSVN commit dialog for the button which invokes the plugin, 
             * e.g., "Choose issue" or "Select ticket". Make sure the string is not too long, otherwise it might not fit into the button. 
             * If the method returns an error (e.g., E_NOTIMPL), a default text is used for the button. */

            return "Gemini";
        }

        public bool ValidateParameters(IntPtr hParentWnd, string parameters)
        {
            return true;
        }

        public string GetCommitMessage(
            IntPtr hParentWnd, string parameters, string commonRoot, string[] pathList, string originalMessage)
        {
            var dummyString = string.Empty;
            var revPropNames = new string[0];
            var revPropValues = new string[0];

            /* This is the main method of the plugin. This method is called from the TortoiseSVN commit dialog when the user clicks on the plugin button. */

            // Just calls the newer GetCommitMessage2 method
            return this.GetCommitMessage2(
                hParentWnd,
                parameters,
                string.Empty,
                commonRoot,
                pathList,
                originalMessage,
                string.Empty,
                out dummyString,
                out revPropNames,
                out revPropValues);
        }

        public string GetCommitMessage2(
            IntPtr hParentWnd,
            string parameters,
            string commonURL,
            string commonRoot,
            string[] pathList,
            string originalMessage,
            string bugID,
            out string bugIDOut,
            out string[] revPropNames,
            out string[] revPropValues)
        {
            /* This method is called from the TortoiseSVN commit dialog when the user clicks on the plugin button. This method is called 
             * instead of GetCommitMessage(). Please refer to the documentation for GetCommitMessage for the parameters that are also used there. */

            bugIDOut = "GEM:1234";
            revPropNames = new string[0];
            revPropValues = new string[0];

            var issues = this.geminiManager.GetIssues();

            var nativeWindow = new NativeWindow();
            var form = new GeminiIssues(issues);

            try
            {
                nativeWindow.AssignHandle(hParentWnd);
            }
            catch (Exception)
            {
                nativeWindow = null;
            }

            if (nativeWindow == null)
            {
                form.ShowDialog();
            }
            else
            {
                form.ShowDialog(nativeWindow);
            }

            // If our dialog result is not ok, just return original message
            if (form.DialogResult != DialogResult.OK || form.SelectedIssue == null) return originalMessage;

            var issue = form.SelectedIssue;
            bugIDOut = string.Concat("GEM:", issue.Id);
            return string.Concat(originalMessage, Environment.NewLine, bugIDOut);
        }

        public string CheckCommit(
            IntPtr hParentWnd, string parameters, string commonURL, string commonRoot, string[] pathList, string commitMessage)
        {
            /* This method is called right before the commit dialog is closed and the commit begins. A plugin can use this method 
             * to validate the selected files/folders for the commit and/or the commit message entered by the user. The parameters 
             * are the same as for GetCommitMessage2(), with the difference that commonURL is now the common URL of all checked 
             * items, and commonRoot the root path of all checked items. */

            return !this.geminiManager.IsConnected() ? "Unable to connect to Gemini, please check the options and ensure Gemini is reachable" : null;
        }

        public string OnCommitFinished(IntPtr hParentWnd, string commonRoot, string[] pathList, string logMessage, int revision)
        {
            /* This method is called after a successful commit. A plugin can use this method to e.g., close the selected issue 
             * or add information about the commit to the issue. The parameters are the same as for GetCommitMessage2. */

            // Apply updates to gemini issues as required (setting status, due date, logging time, resolution etc)
            // If there is an error, we can return an string which will be displayed to the user
            return null;
        }

        public bool HasOptions()
        {
            /* This method is called from the settings dialog where the user can configure the plugins. If a plugin provides its 
             * own configuration dialog with ShowOptionsDialog, it must return TRUE here, otherwise it must return FALSE. */
            return true;
        }

        public string ShowOptionsDialog(IntPtr hParentWnd, string parameters)
        {
            /* This method is called from the settings dialog when the user clicks on the "Options" button that is shown if HasOptions returns TRUE. 
             * A plugin can show an options dialog to make it easier for the user to configure the plugin. */

            var nativeWindow = new NativeWindow();
            var login = new FormOptions();

            try
            {
                nativeWindow.AssignHandle(hParentWnd);
            }
            catch (Exception)
            {
                nativeWindow = null;
            }

            if (nativeWindow == null)
            {
                login.ShowDialog();
            }
            else
            {
                login.ShowDialog(nativeWindow);
            }

            // return parameters string as Json
            var result = JsonConvert.SerializeObject(geminiConnection);

            return result;
        }
    }
}
