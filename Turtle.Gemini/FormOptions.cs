﻿namespace Turtle.Gemini
{
    using System;
    using System.Windows.Forms;

    using Turtle.Gemini.Settings;

    public partial class FormOptions : Form
    {
        private AppSettings appSettings;

        public FormOptions()
        {
            InitializeComponent();
            Load += OnLoad;
        }

        private void OnLoad(object sender, EventArgs eventArgs)
        {
            appSettings = new AppSettings();

            txtGeminiUrl.DataBindings.Add(new Binding("Text", appSettings, AppSettings.UrlName));

            chkWindowsAuthentication.DataBindings.Add("Checked", appSettings, AppSettings.WinAuthName);
            txtUsername.DataBindings.Add("ReadOnly", appSettings, AppSettings.WinAuthName);
            txtApiKey.DataBindings.Add("ReadOnly", appSettings, AppSettings.WinAuthName);

            txtUsername.DataBindings.Add(new Binding("Text", appSettings, AppSettings.UserName));
            txtApiKey.DataBindings.Add(new Binding("Text", appSettings, AppSettings.ApiKeyName));
        }

        private void ButtonOkClick(object sender, EventArgs e)
        {
            try
            {
                appSettings.Save();
                this.DialogResult = DialogResult.OK;
            }
            catch (UriFormatException ex)
            {
                MessageBox.Show("Url is not valid, please check and correct", "Invalid Url", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.DialogResult = DialogResult.None;
            }
        }

        private void ButtonCancelClick(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
