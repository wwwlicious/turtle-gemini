﻿//  Copyright 2013 Scott Mackay
//   
//  Licensed under the Apache License, Version 2.0 (the "License"); you may not use 
//  this file except in compliance with the License. You may obtain a copy of the 
//  License at 
//  
//      http://www.apache.org/licenses/LICENSE-2.0 
//  
//  Unless required by applicable law or agreed to in writing, software distributed 
//  under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
//  CONDITIONS OF ANY KIND, either express or implied. See the License for the 
//  specific language governing permissions and limitations under the License.
namespace Turtle.Gemini.Settings
{
    using System;
    using System.Configuration;

    internal class AppSettings : ApplicationSettingsBase
    {
        public const string UrlName = "GeminiUrl";

        public const string UserName = "GeminiUsername";

        public const string ApiKeyName = "GeminiApiKey";

        public const string WinAuthName = "UseWindowsAuthentication";

        // Shared secret is used for encryption
        // You can change this according to your preference
        private const string SharedSecret = "B88CB6F1as";

        [UserScopedSetting]
        public string GeminiUrl
        {
            get
            {
                try
                {
                    // Crypto is the utility class that holds the encryption logic
                    // You can use your own encryption utility class for more control
                    return Crypto.DecryptStringAES((string)this[UrlName], SharedSecret);
                }
                catch
                {
                    // simply return nothing in case of exception
                    return string.Empty;
                }
            }

            set
            {
                this[UrlName] = Crypto.EncryptStringAES(value, SharedSecret);
            }
        }

        [UserScopedSetting]
        public bool UseWindowsAuthentication
        {
            get
            {
                try
                {
                    return (bool)this[WinAuthName];
                }
                catch
                {
                    // simply return nothing in case of exception
                    return true;
                }
            }

            set
            {
                this[WinAuthName] = value;
            }
        }

        [UserScopedSetting]
        public string GeminiUsername
        {
            get
            {
                try
                {
                    // Crypto is the utility class that holds the encryption logic
                    // You can use your own encryption utility class for more control
                    return Crypto.DecryptStringAES((string)this[UserName], SharedSecret);
                }
                catch
                {
                    // simply return nothing in case of exception
                    return string.Empty;
                }
            }

            set
            {
                this[UserName] = Crypto.EncryptStringAES(value, SharedSecret);
            }
        }

        [UserScopedSetting]
        public string GeminiApiKey
        {
            get
            {
                // this part is necessary 
                // for the first time when there is still nothing to
                // read in the settings file. In other words, the
                // "ApiKey" is null or empty.
                try
                {
                    // Crypto is the utility class that holds the encryption logic
                    // You can use your own encryption utility class for more control
                    return Crypto.DecryptStringAES((string)this[ApiKeyName], SharedSecret);
                }
                catch
                {
                    // simply return nothing in case of exception
                    return string.Empty;
                }
            }

            set
            {
                // When you save the settings, the password will be encrypted
                this[ApiKeyName] = Crypto.EncryptStringAES(value, SharedSecret);
            }
        }
    }
}