﻿namespace Turtle.Gemini
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    using Turtle.Gemini.Models;

    public partial class GeminiIssues : Form
    {
        public GeminiIssues(IEnumerable<GeminiIssue> issues)
        {
            InitializeComponent();

            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.ShowIcon = true;
            this.ShowInTaskbar = false;

            listView1.FullRowSelect = true;
            listView1.MultiSelect = false;
            listView1.HeaderStyle = ColumnHeaderStyle.Nonclickable;

            if (SelectedIssue == null)
            {
                ClearControls();
            }

            var items = listView1.Items;

            foreach (var geminiIssue in issues)
            {
                var item = CreateListViewItem(geminiIssue);
                items.Add(item);
            }
        }

        public GeminiIssue SelectedIssue { get; private set; }

        private static ListViewItem CreateListViewItem(GeminiIssue geminiIssue)
        {
            var item = new ListViewItem(string.Format("{0}:{1}", geminiIssue.Code, geminiIssue.Id));
            item.ToolTipText = geminiIssue.Title;

            item.SubItems.Add(geminiIssue.Title);
            item.SubItems.Add(geminiIssue.Type);
            item.SubItems.Add(geminiIssue.Priority);
            item.SubItems.Add(geminiIssue.Status);
            item.SubItems.Add(geminiIssue.DueDate.HasValue ? geminiIssue.DueDate.Value.ToShortDateString() : string.Empty);
            item.SubItems.Add(geminiIssue.Resolution);

            item.Tag = geminiIssue;
            return item;
        }

        private void ClearControls()
        {
            // Hide controls which are not yet implemented
            dtDue.Visible = false;
            chkComplete.Visible = false;
            lblDue.Visible = false;

            tableLayoutPanel1.Visible = false;
        }

        private void ListView1ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            SelectedIssue = e.Item.Tag as GeminiIssue;
            if (this.SelectedIssue == null)
            {
                return;
            }

            tableLayoutPanel1.Visible = true;

            this.lblCode.Text = string.Format("{0}:{1}", this.SelectedIssue.Code, this.SelectedIssue.Id);
            this.lblTitle.Text = this.SelectedIssue.Title;
            this.dtDue.Value = this.SelectedIssue.DueDate.GetValueOrDefault(DateTime.Today);
            this.chkComplete.Checked = false;
        }

        private void BtnSelectClick(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
