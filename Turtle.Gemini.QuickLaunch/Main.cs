﻿namespace Turtle.Gemini.QuickLaunch
{
    using System;
    using System.Windows.Forms;

    using Newtonsoft.Json;

    using Turtle.Gemini.Models;

    public partial class Main : Form
    {
        private readonly GeminiBugTraqProvider gemProvider;
        private GeminiManager manager;

        public Main()
        {
            InitializeComponent();
            gemProvider = new GeminiBugTraqProvider();
        }

        private void OptionsClick(object sender, EventArgs e)
        {
            if (gemProvider.HasOptions())
            {
                var parameters = "";
                parameters = gemProvider.ShowOptionsDialog(this.Handle, parameters);

                var connection = JsonConvert.DeserializeObject<GeminiConnection>(parameters);
                manager = new GeminiManager(connection);

                var connected = manager.IsConnected();
                if (connected)
                {
                    MessageBox.Show(
                        "Successfully connected to Gemini",
                        "Connection Established",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(
                        "Unable to connect to Gemini", "Connection Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show(
                    "Connection to Gemini was cancelled",
                    "Connection Cancelled",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
        }

        private void IssuesClick(object sender, EventArgs e)
        {
            var parameters = "A";
            var orgiBugId = "1";
            string bugId;
            var propNames = new string[0];
            var propValues = new string[0];

            var result = gemProvider.GetCommitMessage2(
                this.Handle,
                parameters,
                "url?",
                "commonRoot?",
                new[] { "a", "b" },
                "msg",
                orgiBugId,
                out bugId,
                out propNames,
                out propValues);
        }

        private void btnCommit_Click(object sender, EventArgs e)
        {
            gemProvider.OnCommitFinished(this.Handle, null, null, null, 1);
        }
    }
}
